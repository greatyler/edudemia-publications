<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_publications',
		'title' => 'Publications',
		'fields' => array (
			array (
				'key' => 'field_56ca39ac4a3b7',
				'label' => 'Publication Year',
				'name' => 'edudms_pub_field_publication_year',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 2017,
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => 4,
			),
			array (
				'key' => 'field_56ca3c4fd0cfd',
				'label' => 'Primary Author',
				'name' => 'primary_author',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_56ca3ba9d0cfc',
				'label' => 'Associated Department Member',
				'name' => 'dept_member',
				'type' => 'user',
				'instructions' => 'Please select a person who this publication will be attributed to. This will make the publication appear on their Publications tab if the department has publications tabs enabled.',
				'role' => array (
					0 => 'all',
				),
				'field_type' => 'select',
				'allow_null' => 1,
			),
			array (
				'key' => 'field_56ca3a8dc3f1b',
				'label' => 'External Links',
				'name' => 'external_links',
				'type' => 'checkbox',
				'choices' => array (
					'amazon' => 'Amazon.com',
					'other' => 'Other Website',
				),
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_56ca3a76c3f1a',
				'label' => 'Amazon Link',
				'name' => 'amazon_link',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_56ca3a8dc3f1b',
							'operator' => '==',
							'value' => 'amazon',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_56ca3b05c3f1c',
				'label' => 'Other Website',
				'name' => 'other_website',
				'type' => 'text',
				'instructions' => 'Please put in the full URL of a website where more information can be found about this publication.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_56ca3a8dc3f1b',
							'operator' => '==',
							'value' => 'other',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'publication',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


















?>