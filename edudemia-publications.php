<?php
/*
Plugin Name: Edudemia Publications Addon
Plugin URI: 
Description: Add publications management to your DMS
Version: 0.1.0
Author: Tyler Pruitt
Author URI:

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/

include(dirname( __FILE__ ) . '/pub_menu_page.php');


// Function to create custom post type: publications
function edudms_pub_create_publications_posttype() {

	register_post_type( 'publication',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Publications' ),
				'singular_name' => __( 'Publication' )
			),
			'public' => true,
			'has_archive' => true,
			'description' => 'Publications published by your department.',
			'rewrite' => array('slug' => 'publication'),
			'supports' => array( 'title' )
		)
	);
}
// Hooking up to theme setup
add_action( 'init', 'edudms_pub_create_publications_posttype' );

function edudms_pub_publications_submenu_settings() {
add_submenu_page('edit.php?post_type=publication', 'edudms_pub_settings', 'Settings', 'manage_options', 'event_settings', 'edudms_pub_menu_render');
}
add_action("admin_menu", 'edudms_pub_publications_submenu_settings');



//Create Pub_type taxonomy

function edudms_pub_create_pub_type() {
 
    $labels = array(
        'name' => __( 'Publication Types', 'taxonomy general name' ),
        'singular_name' => __( 'Publication Type', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Product Features' ),
        'all_items' => __( 'All Product Features' ),
        'parent_item' => __( 'Parent Product Feature' ),
        'parent_item_colon' => __( 'Parent Product Feature:' ),
        'edit_item' => __( 'Edit Product Feature' ), 
        'update_item' => __( 'Update Product Feature' ),
        'add_new_item' => __( 'Add New Product Feature' ),
        'new_item_name' => __( 'New Type' ),
        'menu_name' => __( 'Edit Types' )
    );  
     
    register_taxonomy( 'edudms_pub_type', array('publication'), array (
                    'labels' => $labels,
                    'hierarchical' =>true,
                    'show_ui' => true,
                    'rewrite' => array( 'slug' => 'edudms_pub_type'),
                    'query_var' => true,
                    'show_in_nav_menus' => true,
                    'public' => true
            ));
			
	wp_insert_term( 'Book', 'edudms_pub_type' );
	wp_insert_term( 'Journal Article', 'edudms_pub_type' );
	wp_insert_term( 'Online Post', 'edudms_pub_type' );
	wp_insert_term( 'Magazine Article', 'edudms_pub_type' );
	wp_insert_term( 'Other', 'edudms_pub_type' );
}


add_action('init', 'edudms_pub_create_pub_type');




?>